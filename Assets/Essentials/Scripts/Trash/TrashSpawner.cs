using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashSpawner : Singleton<TrashSpawner>
{
    // Start is called before the first frame update
    [SerializeField] private Transform[] m_SpawnPoints;
    [SerializeField] public  Transform m_ShopperSpawnpoint;
    [SerializeField] public Transform m_ShopperEndpoint;
    [SerializeField] private GameObject m_Target;
    [SerializeField] private float m_LeftLimit;
    [SerializeField] private float m_RightLimit;
    [SerializeField] private float m_FollowSpeed;
    [SerializeField] private float m_Minwaittime;
    [SerializeField] private float m_Maxwaittime;
    [SerializeField] private float m_MinwaittimeShopper;
    [SerializeField] private float m_MaxwaittimeShopper;

    private Vector3 m_Initialpos;
    public int TotalTrashatatime;


    void Start()
    {
        m_Initialpos = this.transform.position;
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetSpawner;
        GameManagerDelegate.OnGameStart += SpawnTrash;
        GameManagerDelegate.OnGameStart += SpawnShopper;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetSpawner;
        GameManagerDelegate.OnGameStart -= SpawnTrash;
        GameManagerDelegate.OnGameStart -= SpawnShopper;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            var Spawnerpos = transform.position;
            Spawnerpos.x = Mathf.Lerp(Spawnerpos.x, m_Target.transform.position.x, m_FollowSpeed * Time.deltaTime);
            Spawnerpos.x = Mathf.Clamp(Spawnerpos.x, m_LeftLimit, m_RightLimit);
            transform.position = Spawnerpos;
        }
    }

    public void SpawnTrash()
    {
        StartCoroutine(ISpawntrash());
    }

    IEnumerator ISpawntrash()
    {
        var trash = PoolManager.Instance.Dequeue(ePoolType.Trash);
        int RandomSpawnPoint = Random.Range(0, m_SpawnPoints.Length);
        trash.transform.position = m_SpawnPoints[RandomSpawnPoint].position;
        trash.SetActive(true);
        LevelManager.Instance.TrashGenerateCount++;
        float waittime = Random.Range(m_Minwaittime, m_Maxwaittime);
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (LevelManager.Instance.TrashGenerateCount < TotalTrashatatime)
            {
                yield return new WaitForSeconds(waittime);
                StartCoroutine(ISpawntrash());
            }
        }
    }

    public void SpawnShopper()
    {
        StartCoroutine(IspawnShopper());
    }

    IEnumerator IspawnShopper()
    {
        var trash = PoolManager.Instance.Dequeue(ePoolType.Shopper);
        trash.transform.position = m_ShopperSpawnpoint.position;
        trash.SetActive(true);
        //LevelManager.Instance.TrashGenerateCount++;
        float waittime = Random.Range(m_MinwaittimeShopper, m_MaxwaittimeShopper);
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {   
                yield return new WaitForSeconds(waittime);
                StartCoroutine(IspawnShopper());
        }
    }

    private void ResetSpawner()
    {
        this.transform.position = m_Initialpos;
    }


}
