using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    // Start is called before the first frame update
    private bool iscollected;
    [SerializeField] private BoxCollider2D m_Collider;
    [SerializeField] private Rigidbody2D m_Rigidbody;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    [SerializeField] private Sprite[] m_TrashSprites;
    [SerializeField] private Vector3 m_InitialScale;
    [SerializeField] private bool canfly;
    [SerializeField] private float m_ShopperSpeed;
    [SerializeField] private Animator m_Anim;
    

    private void OnEnable()
    {
        OnenableTrash();
        GameManagerDelegate.OnGameReset += ResetTrash;

        if (m_Anim != null)
            m_Anim.enabled = true;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetTrash;
    }

    // Update is called once per frame
    void Update()
    {
        if (iscollected == false)
        {
            if (canfly)
            {


                var shopperpos = transform.parent.position;
                shopperpos.x = Mathf.MoveTowards(shopperpos.x, TrashSpawner.Instance.m_ShopperEndpoint.position.x, m_ShopperSpeed * Time.deltaTime);
                transform.parent.position = shopperpos;

                if (transform.parent.position.x == TrashSpawner.Instance.m_ShopperEndpoint.position.x)
                {
                    this.transform.parent.gameObject.SetActive(false);

                }

            }
        }

        if (iscollected)
        {
            //m_Rigidbody.velocity = Vector2.zero;
            if (m_Anim != null)
                m_Anim.enabled = false;
            this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector3.zero, GameConfig.Instance.DecreaseSizeSpeed * Time.deltaTime);
            transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.Player.CollectionPoint.position, GameConfig.Instance.MoveSpeed * Time.deltaTime);

            if (this.transform.localScale == Vector3.zero)
            {
                iscollected = false;
                this.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator TrashCollected()
    {
       
        m_Rigidbody.gravityScale = 0f;
        yield return new WaitForSeconds(0.1f);
        m_Rigidbody.velocity = Vector2.zero;
        yield return new WaitForSeconds(0.1f);
        iscollected = true;
        SoundManager.Instance.SuctionSound();
        LevelManager.Instance.TaskDoneCount++;
        LevelManager.Instance.TrashGenerateCount--;
        HUDManager.Instance.TaskDoneCount.text = LevelManager.Instance.TaskDoneCount.ToString();

        HUDManager.Instance.SetTaskCollected(LevelManager.Instance.TaskDoneCount, LevelManager.Instance.TotalTaskToDo);

        if(LevelManager.Instance.TaskDoneCount == LevelManager.Instance.TotalTaskToDo)
        {
            GameManager.Instance.GameComplete();
        }
        if(LevelManager.Instance.TrashGenerateCount < TrashSpawner.Instance.TotalTrashatatime)
        {
            TrashSpawner.Instance.SpawnTrash();
        }
    }

    void OnenableTrash()
    {
        m_Collider.enabled = true;
        int randomnumber = Random.Range(0, m_TrashSprites.Length);
        m_SpriteRenderer.sprite = m_TrashSprites[randomnumber];
        if (!canfly)
        {
            m_Rigidbody.gravityScale = 1f;
        }
        this.transform.localScale = m_InitialScale;
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(nameof(eTagType.Collector)))
        {
            m_Collider.enabled = false;
            StartCoroutine(TrashCollected());
            
        }
    }

    void ResetTrash()
    {
        this.gameObject.SetActive(false);
    }
}
