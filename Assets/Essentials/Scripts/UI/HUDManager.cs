using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager>
{
    #region Data
    [Header("Player Input")]
    [SerializeField] private ExtendedButton m_InputButtonRight;
    [SerializeField] private ExtendedButton m_InputButtonLeft;
    [SerializeField] private ExtendedButton m_JumpButton;
    [SerializeField] private ExtendedButton m_CollectButton;

    [Space]
    [Header("GamePanels")]
    [SerializeField] private GameObject [] m_GameCompletePanel;
    [SerializeField] private GameObject []m_GameFailPanel;
    [SerializeField] private GameObject m_GameStartPanel;
    [SerializeField] private GameObject[] m_GameGuidePanel;
    [SerializeField] private GameObject Smoke;
    [SerializeField] private GameObject m_LevelSelectionPanel;

    [Space]
    [Header("GamePanelButtons")]
    [SerializeField] private ExtendedButton m_StartButton;
    [SerializeField] private ExtendedButton m_ResetButton;
    [SerializeField] private ExtendedButton m_NextButton;
    [SerializeField] private ExtendedButton m_GameGuideNextButton;
    [SerializeField] private ExtendedButton m_GameGuideBackButton;
    [SerializeField] private ExtendedButton m_MainMenuCompleteButton;
    [SerializeField] private ExtendedButton m_MainMenuFailButton;

    [SerializeField] private ExtendedButton m_ResetButtonL2;
    [SerializeField] private ExtendedButton m_NextButtonL2;
    [SerializeField] private ExtendedButton m_GameGuideNextButtonL2;
    [SerializeField] private ExtendedButton m_GameGuideBackButtonL2;
    [SerializeField] private ExtendedButton m_MainMenuCompleteButtonL2;
    [SerializeField] private ExtendedButton m_MainMenuFailButtonL2;
    [SerializeField] private Image m_CollectBImage;
    [SerializeField] private Sprite m_CollectSprite;
    [SerializeField] private Sprite m_WaterSprite;



    [Space]
    [Header("Task")]
    public TextMeshProUGUI TotalTaskCount;
    public TextMeshProUGUI TaskDoneCount;

    [Space]
    [Header("Timer")]
    public Image TimerFillImage;
    public Transform m_TimerNeedle;

    [Space]
    [Header("Character Selection")]

    [SerializeField] private ExtendedButton m_BoySelect;
    [SerializeField] private ExtendedButton m_GirlSelect;
    [SerializeField] private GameObject m_CharacterselectionPanel;

    [Space]
    [Header("Main Menu")]
    [SerializeField] private ExtendedButton m_PlayMainMenuButton;
    [SerializeField] private GameObject m_MainMenuPanel;


    [Space]
    [Header("Progress Bar")]
    [SerializeField] private Image m_ProgressBar;

    [Space]
    [Header("level Titles")]
    [SerializeField] private GameObject [] m_Level1Title;

    [Space]
    [Header("Sound")]
    public ExtendedButton SoundButton;
    [SerializeField] private Color m_DisableSoundColor;
    [SerializeField] private Color m_NormalSoundColor;

    [Space]
    [Header("Privacy Policy / Credits")]

    [SerializeField] private ExtendedButton m_CreditsButton;
    [SerializeField] private ExtendedButton m_PrivacyPolicyButton;
    [SerializeField] private ExtendedButton m_WebsiteLinkButton;
    [SerializeField] private ExtendedButton m_InstaLinkButton;
    [SerializeField] private GameObject m_CreditsPanel;
    [SerializeField] private string m_PrivacyLink;
    [SerializeField] private string m_WebsiteLink;
    [SerializeField] private string m_InstagramLink;

    [Space]
    [Header("Size Objects")]
    public GameObject CompleteSizeObject;
    public GameObject FailSizeObject;
    public GameObject GuideSizeObject;
    public GameObject TopPanel;
    [Space]
    [Header("Volume Slider")]
    public Slider BGSlider;
    public Slider SfxSlider;

    [Space]
    [Header("Settings")]
    [SerializeField] private GameObject m_SettingPanel;
    [SerializeField] private ExtendedButton m_SettingButton;

    [Space]
    [Header("Pause")]

    [SerializeField] private GameObject m_PausePanel;
    [SerializeField] private ExtendedButton m_PauseButton;
    [SerializeField] private ExtendedButton m_MainMenuButtonPause;
    [SerializeField] private ExtendedButton m_ResumeButtonPause;

    #endregion Data


    private void OnEnable()
    {
        if (StorageManager.Instance.Sound == 1)
        {
            SoundManager.Instance.SoundOFF();
            SoundButton.image.color = m_DisableSoundColor;
        }
        else
        {
            SoundManager.Instance.SoundOn();
            SoundButton.image.color = m_NormalSoundColor;
        }
        BGSlider.value = 1;
        SfxSlider.value = 1;
        BGControl();
        SFXControl();
       

        var compsize = GameManager.Instance.Remaping(Screen.width, 1024, 3040, 0.6f, 1);
        var Guidesize = GameManager.Instance.Remaping(Screen.width, 1024, 3040, 0.8f, 1);
        CompleteSizeObject.transform.localScale = new Vector3(compsize, compsize, compsize);
        FailSizeObject.transform.localScale = new Vector3(compsize, compsize, compsize);
        GuideSizeObject.transform.localScale = new Vector3(Guidesize, Guidesize, Guidesize);
        TopPanel.transform.localScale = new Vector3(compsize, compsize, compsize);

        m_InputButtonLeft.OnDownEvent += InputDownLeft;
        m_InputButtonLeft.OnUpEvent += InputUpLeft;

        m_InputButtonRight.OnDownEvent += InputDownRight;
        m_InputButtonRight.OnUpEvent += InputUpRight;

        m_JumpButton.OnDownEvent +=Jump;

        m_CollectButton.OnDownEvent += CollectButtonDown;
        m_CollectButton.OnUpEvent += CollectButtonUp;

        m_StartButton.onClick.AddListener(OnClickStartButton);
        m_ResetButton.onClick.AddListener(ClickReset);
        m_NextButton.onClick.AddListener(ClickNext);

        m_ResetButtonL2.onClick.AddListener(ClickReset);
        m_NextButtonL2.onClick.AddListener(ClickNext);

        m_PlayMainMenuButton.onClick.AddListener(ClickPlayButton);

        m_GameGuideNextButton.onClick.AddListener(ClickGuidePanelNext);
        m_GameGuideBackButton.onClick.AddListener(ClickGuidePanelBack);

        m_GameGuideNextButtonL2.onClick.AddListener(ClickGuidePanelNext);
        m_GameGuideBackButtonL2.onClick.AddListener(ClickGuidePanelBack);

        m_MainMenuCompleteButton.onClick.AddListener(CLickMainMenuButton);
        m_MainMenuFailButton.onClick.AddListener(CLickMainMenuButton);

        m_MainMenuCompleteButtonL2.onClick.AddListener(CLickMainMenuButton);
        m_MainMenuFailButtonL2.onClick.AddListener(CLickMainMenuButton);

        SoundButton.onClick.AddListener(SoundOnOFF);

        m_CreditsButton.onClick.AddListener(OpenCreditsPanel);
        m_WebsiteLinkButton.onClick.AddListener(OpenWebsite);
        m_InstaLinkButton.onClick.AddListener(OpenInstagram);
        m_PrivacyPolicyButton.onClick.AddListener(OpenPrivacyPolicy);
        m_SettingButton.onClick.AddListener(OpenSettingPanel);
        m_PauseButton.onClick.AddListener(OpenPausePanel);
        m_ResumeButtonPause.onClick.AddListener(ResumeGame);
        m_MainMenuButtonPause.onClick.AddListener(MainmenuPause);


        GameManagerDelegate.OnGameReset += OnClickResetButton;
        GameManagerDelegate.OnGameFail += ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete += ShowGameCompletePanel;


    }

    private void OnDisable()
    {
        m_InputButtonLeft.OnDownEvent -= InputDownLeft;
        m_InputButtonLeft.OnUpEvent -= InputUpLeft;

        m_InputButtonRight.OnDownEvent -= InputDownRight;
        m_InputButtonRight.OnUpEvent -= InputUpRight;

        m_JumpButton.OnDownEvent -= Jump;

        m_CollectButton.OnDownEvent -= CollectButtonDown;
        m_CollectButton.OnUpEvent -= CollectButtonUp;

        m_StartButton.onClick.RemoveListener(OnClickStartButton);
        m_ResetButton.onClick.RemoveListener(ClickReset);
        m_NextButton.onClick.RemoveListener(ClickNext);

        m_ResetButtonL2.onClick.RemoveListener(ClickReset);
        m_NextButtonL2.onClick.RemoveListener(ClickNext);

        m_PlayMainMenuButton.onClick.RemoveListener(ClickPlayButton);

        m_GameGuideNextButton.onClick.RemoveListener(ClickGuidePanelNext);
        m_GameGuideBackButton.onClick.RemoveListener(ClickGuidePanelBack);

        m_GameGuideNextButtonL2.onClick.RemoveListener(ClickGuidePanelNext);
        m_GameGuideBackButtonL2.onClick.RemoveListener(ClickGuidePanelBack);

        m_MainMenuCompleteButton.onClick.RemoveListener(CLickMainMenuButton);
        m_MainMenuFailButton.onClick.RemoveListener(CLickMainMenuButton);

        m_MainMenuCompleteButtonL2.onClick.RemoveListener(CLickMainMenuButton);
        m_MainMenuFailButtonL2.onClick.RemoveListener(CLickMainMenuButton);


        SoundButton.onClick.RemoveListener(SoundOnOFF);

        m_CreditsButton.onClick.RemoveListener(OpenCreditsPanel);
        m_WebsiteLinkButton.onClick.RemoveListener(OpenWebsite);
        m_InstaLinkButton.onClick.RemoveListener(OpenInstagram);
        m_PrivacyPolicyButton.onClick.RemoveListener(OpenPrivacyPolicy);
        m_SettingButton.onClick.RemoveListener(OpenSettingPanel);
        m_PauseButton.onClick.RemoveListener(OpenPausePanel);
        m_ResumeButtonPause.onClick.RemoveListener(ResumeGame);
        m_MainMenuButtonPause.onClick.RemoveListener(MainmenuPause);

        GameManagerDelegate.OnGameReset -= OnClickResetButton;
        GameManagerDelegate.OnGameFail -= ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete -= ShowGameCompletePanel;

    }

    private void Update()
    {
        
    }


    #region PlayerInputRegion
    void InputDownLeft(PointerEventData pointer)
    {
        GameManager.Instance.Player.MoveLeft();
    }

    void InputUpLeft(PointerEventData pointer)
    {
        GameManager.Instance.Player.StopMoving();
    }

    void InputDownRight(PointerEventData pointer)
    {
        GameManager.Instance.Player.MoveRight();
    }

    void InputUpRight(PointerEventData pointer)
    {
        GameManager.Instance.Player.StopMoving();
    }

    void Jump(PointerEventData pointer)
    {
        GameManager.Instance.Player.Jump();
    }

    void CollectButtonDown(PointerEventData pointer)
    {
        GameManager.Instance.Player.StartCollectingGarbage();
    }

    void CollectButtonUp(PointerEventData pointer)
    {
        GameManager.Instance.Player.StopCollectingGarbage();
    }

    public void ChangeCollect_WaterImage()
    {
        if (StorageManager.Instance.CurrentlevelNo == 0)
        {
            m_CollectBImage.sprite = m_CollectSprite;
        }
        else
        {
            m_CollectBImage.sprite = m_WaterSprite;
        }
    }

    #endregion PlayerInputRegion


    #region GameStates

    void OnClickStartButton()
    {

        StartCoroutine(ClickStartButton());
    }

    IEnumerator ClickStartButton()
    {
        SoundManager.Instance.ButtonClickSound();
        m_GameStartPanel.SetActive(false);
        yield return new WaitForSeconds(0f);
        GameManager.Instance.Gamestart();

    }

    void OnClickResetButton()
    {
        m_ProgressBar.fillAmount = 0;
        StartCoroutine(ClickResetButton());
    }

    IEnumerator ClickResetButton()
    {
        //yield return new WaitForSeconds(0.1f);
        //if (m_GameFailPanel[StorageManager.Instance.CurrentlevelNo].activeSelf)
        //{
        //    m_GameFailPanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        //}
        //if (m_GameCompletePanel[StorageManager.Instance.CurrentlevelNo].activeSelf)
        //{
        //    m_GameCompletePanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        //}

        for(int i =0; i<m_GameFailPanel.Length; i++)
        {
            m_GameCompletePanel[i].SetActive(false);
            m_GameFailPanel[i].SetActive(false);
        }
        yield return new WaitForSeconds(0f);
        

    }

    void ShowGameCompletePanel()
    {
        StartCoroutine(GameCompletePanel());
    }

    IEnumerator GameCompletePanel()
    {
        yield return new WaitForSeconds(2f);
        SoundManager.Instance.GameCompleteSound();
        m_GameCompletePanel[StorageManager.Instance.CurrentlevelNo].SetActive(true);
        StorageManager.Instance.CurrentlevelNo++;
        if (StorageManager.Instance.CurrentlevelNo > 1)
        {
            StorageManager.Instance.CurrentlevelNo = 0;
        }
    }



    void ShowGameFailPanel()
    {
        StartCoroutine(GameFailPanel());
    }

    IEnumerator GameFailPanel()
    {
        yield return new WaitForSeconds(2f);
        SoundManager.Instance.GameFailSound();
        m_GameFailPanel[StorageManager.Instance.CurrentlevelNo].SetActive(true);
    }

    void ClickReset()
    {
        SoundManager.Instance.ButtonClickSound();
        m_GameGuidePanel[StorageManager.Instance.CurrentlevelNo].SetActive(true);
        GameManager.Instance.GameReset();
    }

    void ClickNext()
    {
        //m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        SoundManager.Instance.ButtonClickSound();
        m_GameGuidePanel[StorageManager.Instance.CurrentlevelNo].SetActive(true);
        GameManager.Instance.GameReset();
    }


    void ClickGuidePanelNext()
    {
        StartCoroutine(EnumClickGuidePanelNext());
    }

    IEnumerator EnumClickGuidePanelNext()
    {
        SoundManager.Instance.ButtonClickSound();
        m_GameStartPanel.SetActive(false);
        Smoke.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        m_GameGuidePanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        yield return new WaitForSeconds(2.5f);
        Smoke.SetActive(false);
        m_Level1Title[StorageManager.Instance.CurrentlevelNo].SetActive(true);
        yield return new WaitForSeconds(3f);
        m_Level1Title[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        GameManager.Instance.Gamestart();
    }

    void ClickGuidePanelBack()
    {
        SoundManager.Instance.ButtonClickSound();
        m_GameGuidePanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        m_CharacterselectionPanel.SetActive(true);
    }

    void CLickMainMenuButton()
    {
        SoundManager.Instance.ButtonClickSound();
        SoundManager.Instance.waterSoundOFF();
        m_GameCompletePanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        m_GameFailPanel[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        m_MainMenuPanel.SetActive(true);
        GameManager.Instance.GameReset();
    }


    #endregion GameStates

    #region GameCharacterSelection

    public void CharacterSelectionNext(int i_CharacterNo)
    {
        
        SoundManager.Instance.ButtonClickSound();
        StorageManager.Instance.CharacterNo = i_CharacterNo;
        m_CharacterselectionPanel.SetActive(false);
        m_GameGuidePanel[StorageManager.Instance.CurrentlevelNo].SetActive(true);
        GameManager.Instance.Player.ResetPlayer();

    }

    public void CharacterSelectionBack()
    {
        SoundManager.Instance.ButtonClickSound();
        m_CharacterselectionPanel.SetActive(false);
        m_MainMenuPanel.SetActive(true);
    }

    #endregion GameCharacterSelection

    #region MainMenu

    public void ClickPlayButton()
    {
        SoundManager.Instance.ButtonClickSound();
        m_LevelSelectionPanel.SetActive(true);
        m_MainMenuPanel.SetActive(false);
    }


    public void SoundOnOFF()
    {
        if(StorageManager.Instance.Sound ==0)
        {
            SoundManager.Instance.SoundOFF();
            SoundButton.image.color = m_DisableSoundColor;
            StorageManager.Instance.Sound = 1;
        }
        else
        {
            SoundManager.Instance.SoundOn();
            SoundButton.image.color = m_NormalSoundColor;
            StorageManager.Instance.Sound = 0;
        }
    }
    #endregion MainMenu

    #region Timer

    public void SetRemainingTime(float i_Amount)
    {
        TimerFillImage.fillAmount = i_Amount;
        m_TimerNeedle.localEulerAngles = new Vector3(0, 0, GameManager.Instance.Remaping(i_Amount, 0, 1, 360, 0));
    }


    #endregion Timer

    #region Task 

    public void SetTaskCollected(int i_TaskCompleted, int i_TaskTotal)
    {
        TaskDoneCount.text = "" + i_TaskCompleted;
        TotalTaskCount.text = "" + i_TaskTotal;


        m_ProgressBar.fillAmount = GameManager.Instance.Remaping(i_TaskCompleted, 0, i_TaskTotal, 0, 1);

    }

    #endregion Task

    #region Privacy and Credits

    void OpenCreditsPanel()
    {
        SoundManager.Instance.ButtonClickSound();
        m_CreditsPanel.SetActive(true);
    }

    void OpenWebsite()
    {
        SoundManager.Instance.ButtonClickSound();
        Application.OpenURL(m_WebsiteLink);
    }

    void OpenInstagram()
    {
        SoundManager.Instance.ButtonClickSound();
        Application.OpenURL(m_InstagramLink);
    }

    void OpenPrivacyPolicy()
    {
        SoundManager.Instance.ButtonClickSound();
        Application.OpenURL(m_PrivacyLink);
    }

    #endregion Privacy and Credits

    #region LevelSelection
    public void LevelSelected(int levelno)
    {
        SoundManager.Instance.ButtonClickSound();
        StorageManager.Instance.CurrentlevelNo = levelno;
        m_CharacterselectionPanel.SetActive(true);
        m_LevelSelectionPanel.SetActive(false);
        LevelManager.Instance.setLevel();
    }

    #endregion LevelSelection


    #region SoundSlider

    public void BGControl()
    {
        var BGvalue = GameManager.Instance.Remaping(HUDManager.Instance.BGSlider.value, 0, 1, 0f, 0.04f);
        SoundManager.Instance.BG.volume = BGvalue;
    }

    public void SFXControl()
    {
       
        var Timervalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.35f);
        SoundManager.Instance.TimerAudiosource.volume = Timervalue;

        var vaccumvalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.1f);
        SoundManager.Instance.VaccumCleanerAudiosource.volume = vaccumvalue;

        var watervalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.3f);
        SoundManager.Instance.WaterAudiosource.volume = watervalue;

        var citysoundvalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.1f);
        SoundManager.Instance.CitySoundAudiosource.volume = citysoundvalue;

        var wateringplantsvalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.3f);
        SoundManager.Instance.WateringPlantsAudiosource.volume = wateringplantsvalue;

        var MainSFXvalue = GameManager.Instance.Remaping(HUDManager.Instance.SfxSlider.value, 0, 1, 0f, 0.8f);
        SoundManager.Instance.SFX.volume = MainSFXvalue;

    }
    #endregion SoundSlider

    #region Setting
    void OpenSettingPanel()
    {
        m_SettingPanel.SetActive(true);
    }
    #endregion Setting

    #region Pause
    void OpenPausePanel()
    {
        
        SoundManager.Instance.waterSoundOFF();
        GameManager.Instance.e_gamestates = eGameStates.Pause;
        m_PausePanel.SetActive(true);
        Time.timeScale = 0f;
    }

    void ResumeGame()
    {

        SoundManager.Instance.waterSoundOn();
        GameManager.Instance.e_gamestates = eGameStates.Playing;
        Time.timeScale = 1f;
        m_PausePanel.SetActive(false);
    }

    void MainmenuPause()
    {
        m_MainMenuPanel.SetActive(true);
        GameManager.Instance.GameReset();
        m_PausePanel.SetActive(false);
        Time.timeScale = 1f;
        SoundManager.Instance.waterSoundOFF();
    }

    #endregion Pause
}
