using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<instance> : MonoBehaviour where instance : Singleton<instance>
{
    public static instance Instance;
    public bool isdontdestroyonload;

    public virtual void Awake()
    {
        if (isdontdestroyonload)
        {
            if (!Instance)
            {
                Instance = this as instance;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Instance = this as instance;
        }
    }
}
