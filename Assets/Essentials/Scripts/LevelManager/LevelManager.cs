using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    #region Data
    public LevelScript[] LevelScripts;
    public LevelScript CurrentLevel;
    public int TotalTaskToDo;
    public int TaskDoneCount;
    public int LevelPlayerId;
    public int TrashGenerateCount;
    public Follower Dog;
    public GameObject TreeObject;
    public GameObject DogObject;
    #endregion Data

    #region Unity Initialization
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += setLevel;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= setLevel;
    }

    private void Start()
    {
       // print("level No" + StorageManager.Instance.CurrentlevelNo);
        //setLevel();
    }
    #endregion Unity Initialization


    public void setLevel()
    {
        closeAllLevels();
        var levelNo = StorageManager.Instance.CurrentlevelNo;

        while (levelNo > LevelScripts.Length - 1)
        {
            levelNo -= LevelScripts.Length;
        }
        TotalTaskToDo = LevelScripts[levelNo].TotalTaskToDo;
        //HUDManager.Instance.TotalTaskCount.text = TotalTaskToDo.ToString();
        TaskDoneCount = 0;

        HUDManager.Instance.SetTaskCollected(TaskDoneCount, TotalTaskToDo);

        //HUDManager.Instance.TaskDoneCount.text = TaskDoneCount.ToString();
        TimeManager.Instance.setTargetTime(LevelScripts[levelNo].LevelTargetTime);
        HUDManager.Instance.TimerFillImage.fillAmount = 1f;
        GameManager.Instance.Player.ResetPlayerPosition();
        if(StorageManager.Instance.CurrentlevelNo == 0)
        {
            Dog.gameObject.SetActive(true);
            Dog.FollowerReset();
            DogObject.SetActive(true);
            TreeObject.SetActive(false);
            SoundManager.Instance.waterSoundOn();
        }
        else
        {
            Dog.gameObject.SetActive(false);
            DogObject.SetActive(false);
            TreeObject.SetActive(true);
            SoundManager.Instance.waterSoundOn();
        }
        HUDManager.Instance.ChangeCollect_WaterImage();
        TrashGenerateCount = 0;
        LevelScripts[levelNo].gameObject.SetActive(true);
        CurrentLevel = LevelScripts[levelNo];


    }

    void closeAllLevels()
    {
        for (int i = 0; i < LevelScripts.Length; i++)
        {
            LevelScripts[i].gameObject.SetActive(false);
        }
    }
}
