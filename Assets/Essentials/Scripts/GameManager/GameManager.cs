using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    #region Data
    [Space]
    public eGameStates e_gamestates = eGameStates.Idle;
    public Player Player;
    #endregion Data


    public void Gamestart()
    {
        GameManager.Instance.e_gamestates = eGameStates.Playing;
        GameManagerDelegate.GameStart();
    }

    public void GameFail()
    {
        GameManager.Instance.e_gamestates = eGameStates.Fail;
        GameManagerDelegate.GameFail();
    }

    public void GameReset()
    {
        GameManager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameReset();
    }

    public void GameComplete()
    {
        GameManager.Instance.e_gamestates = eGameStates.Complete;
        GameManagerDelegate.GameComplete();
        
    }

    public void GameNext()
    {
        GameManager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameNext();
    }

    public void GamePause()
    {
        GameManager.Instance.e_gamestates = eGameStates.Pause;
    }

    public float Remaping(float i_From, float i_FromMin, float i_FromMax, float i_ToMin, float i_ToMax)
    {
        float fromAbs = i_From - i_FromMin;
        float fromMaxAbs = i_FromMax - i_FromMin;

        float normal = fromAbs / fromMaxAbs;

        float toMaxAbs = i_ToMax - i_ToMin;
        float toAbs = toMaxAbs * normal;

        float to = toAbs + i_ToMin;

        return to;
    }
}

