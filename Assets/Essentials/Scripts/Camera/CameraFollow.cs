using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private float interpVelocity;
    public GameObject target;
    Vector3 targetPos;
    // Use this for initialization
    void Start()
    {
        targetPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);

            interpVelocity = targetDirection.magnitude * 5f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            //transform.position = Vector3.Lerp(transform.position, targetPos + offset, 0.25f);
            var camerapos = transform.position;
            camerapos.x = Mathf.Lerp(camerapos.x, targetPos.x, GameConfig.Instance.FollowSpeed * Time.deltaTime);
            camerapos.x = Mathf.Clamp(camerapos.x,GameConfig.Instance.LeftXLimit,GameConfig.Instance.RightXLimit);
            transform.position = camerapos;
        

        }
    }
}
