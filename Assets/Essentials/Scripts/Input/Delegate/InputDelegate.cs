public class InputDelegate
{
    public delegate void OnInputDelegate();
    public static OnInputDelegate OnFingerSwipe;


    public static void Swipe_Direction()
    {
        if (OnFingerSwipe != null)
        {
            OnFingerSwipe.Invoke();
        }
    }
}
