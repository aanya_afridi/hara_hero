using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour
{
    #region Data
    public static StorageManager Instance;
    public int CurrentlevelNo { get { return PlayerPrefs.GetInt(nameof(CurrentlevelNo),0); } set { PlayerPrefs.SetInt((nameof(CurrentlevelNo)), value);}}
    public int CharacterNo { get { return PlayerPrefs.GetInt(nameof(CharacterNo), 0); } set { PlayerPrefs.SetInt((nameof(CharacterNo)), value); } }
    public int Sound { get { return PlayerPrefs.GetInt(nameof(Sound), 0); } set { PlayerPrefs.SetInt((nameof(Sound)), value); } }
    #endregion Data

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
