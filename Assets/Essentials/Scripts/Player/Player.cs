using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Data
    [SerializeField] private BoxCollider2D m_Collider;
    [SerializeField] private Rigidbody2D m_Rigidbody;
    [HideInInspector]public bool ismoveright;
    [HideInInspector]public bool ismoveleft;
    public Transform CollectionPoint;
    [SerializeField] private GameObject [] Collector;
    private Vector3 m_playerposition;
    private Quaternion m_PlayerRotation;
    public Ikplayer[] BoyplayerIk;
    public Ikplayer[] GirlplayerIk;
    private bool canjump;

    #endregion Data
    private void Start()
    {
        m_playerposition = this.transform.position;
        m_PlayerRotation = this.transform.rotation;
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetPlayerObejcts;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetPlayerObejcts;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {

            if (ismoveright == true && ismoveleft == false)
            {
                this.transform.rotation = new Quaternion(0, 0, 0, 0);
                m_Rigidbody.velocity = new Vector2(GameConfig.Instance.MovementSpeed * 1, m_Rigidbody.velocity.y);
                //m_Animator.SetBool(nameof(eAnimStates.Run), true);
                if (StorageManager.Instance.CharacterNo == 0)
                {
                    BoyplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
                }
                else
                {
                    GirlplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
                }

            }

            if (ismoveright == false && ismoveleft == true)
            {
                this.transform.rotation = new Quaternion(0, 180, 0, 0);

                m_Rigidbody.velocity = new Vector2(GameConfig.Instance.MovementSpeed * -1, m_Rigidbody.velocity.y);
                //m_Animator.SetBool(nameof(eAnimStates.Run), true);
                if (StorageManager.Instance.CharacterNo == 0)
                {
                    BoyplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
                }
                else
                {
                    GirlplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
                }

            }
        }
        
    }

    #region Movement

    public void MoveRight()
    {

        ismoveright = true;
    }

    public void MoveLeft()
    {
        ismoveleft = true;
    }

    public void StopMoving()
    {
        ismoveleft = false;
        ismoveright = false;
        var Pvelocity = m_Rigidbody.velocity;
        Pvelocity.x = 0;
        m_Rigidbody.velocity = Pvelocity;
        //m_Animator.SetBool(nameof(eAnimStates.Run), false);
        if (StorageManager.Instance.CharacterNo == 0)
        {
            BoyplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        }
        else
        {
            GirlplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        }
    }

    public void Jump()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (!canjump)
            {
                canjump = true;
                if (StorageManager.Instance.CharacterNo == 0)
                {
                    BoyplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetTrigger(nameof(eAnimStates.Jump));
                }
                else
                {
                    GirlplayerIk[LevelManager.Instance.LevelPlayerId].PlayerAnimator.SetTrigger(nameof(eAnimStates.Jump));
                }
                m_Rigidbody.AddForce(Vector2.up * GameConfig.Instance.JumpForce);
                StartCoroutine(IEnumCanJump());
            }
        }
    }
    IEnumerator IEnumCanJump()
    {
        yield return new WaitForSeconds(1.4f);
        canjump = false;

    }

    #endregion Movement

    #region GameStates

    public void StartCollectingGarbage()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            SoundManager.Instance.VaccumSoundOn();
            Collector[StorageManager.Instance.CurrentlevelNo].SetActive(true);
            if (StorageManager.Instance.CharacterNo == 0)
            {
                BoyplayerIk[LevelManager.Instance.LevelPlayerId].MoveToCollectionTargetIk();
            }
            else
            {
                GirlplayerIk[LevelManager.Instance.LevelPlayerId].MoveToCollectionTargetIk();
            }
            //m_VaccumParticle.Play();

            if (StorageManager.Instance.CharacterNo == 0)
            {
                BoyplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[StorageManager.Instance.CurrentlevelNo].Play();
            }
            else
            {
                GirlplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[StorageManager.Instance.CurrentlevelNo].Play();
            }
        }
    }

    public void StopCollectingGarbage()
    {
        Collector[0].SetActive(false);
        Collector[1].SetActive(false);
        SoundManager.Instance.VaccumSoundOFF();
        if (StorageManager.Instance.CharacterNo == 0)
        {
            BoyplayerIk[LevelManager.Instance.LevelPlayerId].MoveToNormalTargetIK();
        }
        else
        {
            GirlplayerIk[LevelManager.Instance.LevelPlayerId].MoveToNormalTargetIK();
        }
        //m_VaccumParticle.Stop();
        if (StorageManager.Instance.CharacterNo == 0)
        {
            BoyplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[StorageManager.Instance.CurrentlevelNo].Stop();
        }
        else
        {
            GirlplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[StorageManager.Instance.CurrentlevelNo].Stop();
        }
    }

    public void ResetPlayer()
    {
        
       
        if (StorageManager.Instance.CharacterNo == 0)
        {
            for (int i = 0; i < BoyplayerIk.Length; i++)
            {
                BoyplayerIk[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < GirlplayerIk.Length; i++)
            {
                GirlplayerIk[i].gameObject.SetActive(false);
            }
            BoyplayerIk[LevelManager.Instance.LevelPlayerId].gameObject.SetActive(true);

        }
        else
        {
            for (int i = 0; i < GirlplayerIk.Length; i++)
            {
                GirlplayerIk[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < BoyplayerIk.Length; i++)
            {
                BoyplayerIk[i].gameObject.SetActive(false);
            }
            GirlplayerIk[LevelManager.Instance.LevelPlayerId].gameObject.SetActive(true);

        }

        this.transform.position = m_playerposition;
        this.transform.rotation = m_PlayerRotation;
    }

    public void ResetPlayerPosition()
    {
        this.transform.position = m_playerposition;
        this.transform.rotation = m_PlayerRotation;
    }

    public void ResetPlayerObejcts()
    {
        for (int i = 0; i < Collector.Length; i++)
        {
            Collector[StorageManager.Instance.CurrentlevelNo].SetActive(false);
        }
        for(int i=0;i< BoyplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle.Length; i++)
        {
            BoyplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[i].Stop();
        }
        for (int i = 0; i < GirlplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle.Length; i++)
        {
            GirlplayerIk[LevelManager.Instance.LevelPlayerId].VaccumParticle[i].Stop();
        }

    }
    #endregion GameStates
}
